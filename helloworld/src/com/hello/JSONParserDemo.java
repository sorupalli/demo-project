package com.hello;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONParserDemo {

	public static void main(String [] args) throws FileNotFoundException, IOException, ParseException {

		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader("D:\\IL-3583\\assessment_data.txt"));
		JSONArray jsonarray = (JSONArray)obj;
		Map <String,Set<String>> dataMap = new HashMap<>();
		for (int i=0; i<jsonarray.size(); i++) {

			JSONObject jsonObject= (JSONObject)jsonarray.get(i);
			String userName = (String) jsonObject.get("userName");
			String courseId = (String) jsonObject.get("courseId");
			String assignmentId = (String) jsonObject.get("assignmentId");
			String assessmentId = (String) jsonObject.get("assessmentId");
			String[] forAssessmentId = assessmentId.split("/");
			String onlyAssessmentId = forAssessmentId[forAssessmentId.length-1];
			
			if(dataMap.get(assignmentId)!=null){
				String inputData = courseId+","+userName+","+assignmentId+","+onlyAssessmentId;
				dataMap.get(assignmentId).add(inputData);
			}else{
				Set<String> assessmentIdSet = new HashSet<>();
				String inputData = courseId+","+userName+","+assignmentId+","+onlyAssessmentId;
				assessmentIdSet.add(inputData);
				dataMap.put(assignmentId, assessmentIdSet);
			}
			
			
		}
		System.out.println(dataMap.keySet().size());
		File input = new File("D:\\IL-3583\\input.txt");
		for (Entry<String, Set<String>> entry : dataMap.entrySet()) {
		    System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
		    
		    Iterator<String> iter = entry.getValue().iterator();
		    while (iter.hasNext()) {
		    	 if(!input.exists()){
				        System.out.println("We had to make a new file.");
				        input.createNewFile();
				    }
				    FileWriter fileWriter = new FileWriter(input, true);
				    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
				    bufferedWriter.write(iter.next().toString()+"\n");
				    bufferedWriter.close();
		    }
		    
		    
		    
		    
		   
		}


	}
}